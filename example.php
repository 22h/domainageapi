<?php

use Iodev\Whois\Factory;
use TwentyTwo\DomainAgeApi\DomainAgeCrawlerFactory;
use TwentyTwo\DomainAgeApi\DomainAgeNullStorage;

require './vendor/autoload.php';

$exampleStorage = new DomainAgeNullStorage();

$factory = new DomainAgeCrawlerFactory(Factory::get());
$crawler = $factory->getDomainAgeCrawler($exampleStorage);

$age = $crawler->checkDomainAge('google.com');
dump($age);
