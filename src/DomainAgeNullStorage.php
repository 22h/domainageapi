<?php

declare(strict_types=1);

namespace TwentyTwo\DomainAgeApi;

use DateTimeImmutable;

class DomainAgeNullStorage implements DomainAgeStorageInterface
{
    public function hasDomain(string $domainName): bool
    {
        return false;
    }

    public function getDomainAge(string $domainName): ?DateTimeImmutable
    {
       return null;
    }

    public function setDomain(string $domainName, ?DateTimeImmutable $age): void
    {
    }
}
