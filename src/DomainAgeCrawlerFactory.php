<?php

declare(strict_types=1);

namespace TwentyTwo\DomainAgeApi;

use Iodev\Whois\Factory;
use Iodev\Whois\Whois;

class DomainAgeCrawlerFactory
{
    private ?Whois $whois = null;

    public function __construct(private readonly Factory $whoisFactory)
    {
    }

    public function getDomainAgeCrawler(DomainAgeStorageInterface $domainAgeStorage): DomainAgeCrawler
    {
        return new DomainAgeCrawler($domainAgeStorage, $this->createWhois());
    }

    private function createWhois(): Whois
    {
        if (is_null($this->whois)) {
            $this->whois = $this->whoisFactory->createWhois();
        }

        return $this->whois;
    }
}
