<?php

declare(strict_types=1);

namespace TwentyTwo\DomainAgeApi;

use DateTimeImmutable;

interface DomainAgeStorageInterface
{
    public function hasDomain(string $domainName): bool;

    public function getDomainAge(string $domainName): ?DateTimeImmutable;

    public function setDomain(string $domainName, ?DateTimeImmutable $age): void;
}
