<?php

declare(strict_types=1);

namespace TwentyTwo\DomainAgeApi;

use DateTimeImmutable;
use Iodev\Whois\Exceptions\WhoisException;
use Iodev\Whois\Whois;

class DomainAgeCrawler
{
    public function __construct(
        private readonly DomainAgeStorageInterface $domainAgeStorage,
        private readonly Whois $whois
    ) {
    }

    public function checkDomainAge(string $domainName): ?DateTimeImmutable
    {
        $domainName = strtolower($domainName);

        if ($this->domainAgeStorage->hasDomain($domainName)) {
            return $this->domainAgeStorage->getDomainAge($domainName);
        }

        $age = $this->calculateAge($domainName);
        $this->domainAgeStorage->setDomain($domainName, $age);

        return $age;
    }

    private function calculateAge(string $domainName): ?DateTimeImmutable
    {
        try {
            $tldInfo = $this->whois->loadDomainInfo($domainName);
        } catch (WhoisException) {
            return null;
        }

        $possibleAge = $tldInfo->creationDate > 0 ? $tldInfo->creationDate : $tldInfo->updatedDate;

        return ($possibleAge > 0) ? (new DateTimeImmutable())->setTimestamp($possibleAge) : null;
    }
}
